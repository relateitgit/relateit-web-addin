﻿Imports GusNet.GusServer
Imports System.IO
Imports System.Runtime.InteropServices
<ComClass(HTTPRequest.ClassId, HTTPRequest.InterfaceId, HTTPRequest.EventsId)> _
Public Class HTTPRequest

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "d4740b89-20d3-47ca-8407-526083058fe4"
    Public Const InterfaceId As String = "7cb6fcd5-affa-442d-abea-1ad873dc3761"
    Public Const EventsId As String = "0f1f1b0f-fab4-4802-b03c-e77081766204"
#End Region

    Private _parameters As New HTTPParameters
    Private _processor As GusNet.GusServer.GusHttpProcessor

#Region "Property EOS"
    '<DispId(1)> _
    ' Public ReadOnly Property EOS As Boolean
    '     Get
    '         Return _sr.EndOfStream
    '     End Get
    ' End Property
#End Region

#Region "Property Route"
    Private _route As String = ""
    <DispId(3)> _
    Public ReadOnly Property Route As String
        Get
            Return _route
        End Get
    End Property
#End Region

#Region "Property Parameters"
    <DispId(4)> _
    Public ReadOnly Property Parameters As HTTPParameters
        Get
            Return _parameters
        End Get
    End Property
#End Region

#Region "URL Route"
    Private _SourceUrl As String = ""
    <DispId(6)> _
    Public ReadOnly Property SourceUrl As String
        Get
            Return _SourceUrl
        End Get
    End Property
#End Region

#Region "Sub New"
    Public Sub New(ByRef Processor As GusNet.GusServer.GusHttpProcessor)
        MyBase.New()

        _processor = Processor
        Dim parameterString As String
        _SourceUrl = Processor.SourceUrl
        _route = StringFunctions.SelectStr2(1, Processor.SourceUrl, "?")

        ' Overfør GET parametre
        parameterString = StringFunctions.SelectStr2(2, Processor.SourceUrl, "?")
        For i As Integer = 1 To SelectCount(parameterString, "&")
            Dim tmp As String = SelectStr2(i, parameterString, "&")
            Dim Key As String = Uri.UnescapeDataString(SelectStr2(1, tmp, "="))
            Dim Value As String = Uri.UnescapeDataString(SelectStr2(2, tmp, "="))
            If Key <> "" Then
                Parameters.AddParameter(Key, Value, False, True, False, False, False)
            End If
        Next

        Dim u As String = System.Text.Encoding.UTF8.GetString(Processor.OriginalRequest)

        ' OVerfør headers inkl. cookies
        For Each item In Processor.RequestHeaders
            If item.Key.ToLower = "cookie" Then
                For i As Integer = 1 To SelectCount(item.Value, ";")
                    Dim cookie = SelectStr2(i, item.Value, ";")
                    Dim CookieKey = SelectStr2(1, cookie, "=")
                    Dim CookieValue = SelectStr2(2, cookie, "=")
                    Parameters.AddParameter(CookieKey, CookieValue, True, False, False, True, False)
                Next
            Else
                Parameters.AddParameter(item.Key, item.Value, False, False, False, True, False)
            End If
        Next
    End Sub

#End Region

#Region "Dispose"
    Public Sub Dispose()
        _parameters.Dispose()
        _parameters = Nothing

    End Sub
#End Region

End Class


