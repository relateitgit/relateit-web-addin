﻿Imports GusNet.GusServer
Imports System.Runtime.InteropServices
Imports System.Runtime.InteropServices.ComTypes
Imports System.IO
<ComClass(HTTPResponse.ClassId, HTTPResponse.InterfaceId, HTTPResponse.EventsId)> _
Public Class HTTPResponse

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "6333559b-584a-4a28-b7f8-45a7d8770ce3"
    Public Const InterfaceId As String = "f66e0e52-87e7-461b-925d-faa539f679cb"
    Public Const EventsId As String = "5f66d9e3-8070-45ac-bc12-a6fba6d54006"
#End Region
    Private _memoryStreamOut As IO.MemoryStream
    Private _sw As IO.StreamWriter
    Private _responseStream As GusOutputStream
    Private _sr As IO.StreamReader
    Private _mimeType As String = "text/html"
    Private _cookies As New List(Of KeyValuePair(Of String, String))
    Private _fileToSend As String = ""
    Private _processor As GusNet.GusServer.GusHttpProcessor
    Private _MimeTypes As New Dictionary(Of String, String)
    Private _IsRedirect As Boolean = False
    Private _RedirectURL As String = ""

#Region "Property FileName"
    <DispId(7)> _
    Public ReadOnly Property FileName As String
        Get
            Return _fileToSend
        End Get
    End Property
#End Region

#Region "Property IsFileResponse"
    <DispId(8)> _
    Public ReadOnly Property IsFileResponse As Boolean
        Get
            Return _fileToSend <> ""
        End Get
    End Property
#End Region

#Region "Sub New"

    Public Sub New(ByRef Processor As GusNet.GusServer.GusHttpProcessor)
        MyBase.New()
        _processor = Processor
        _responseStream = Processor.OutputStream
        _memoryStreamOut = New IO.MemoryStream
        _sw = New IO.StreamWriter(_memoryStreamOut)

        _MimeTypes.Add("htm", "text/html")
        _MimeTypes.Add("html", "text/html")
        _MimeTypes.Add("xml", "application/xml")
        _MimeTypes.Add("txt", "text/plain")
        _MimeTypes.Add("css", "text/css")

        _MimeTypes.Add("js", "application/javascript")
        _MimeTypes.Add("json", "application/json")
        _MimeTypes.Add("swf", "application/x-shockwave-flash")
        _MimeTypes.Add("flv", "video/x-flv")

        'images
        _MimeTypes.Add("png", "image/png")
        _MimeTypes.Add("gif", "image/gif")
        _MimeTypes.Add("jpg", "image/jpg")
        _MimeTypes.Add("jpeg", "image/jpeg")
        _MimeTypes.Add("jpe", "image/jpeg")
        _MimeTypes.Add("bmp", "image/bmp")
        _MimeTypes.Add("ico", "image/vnd.microsoft.icon")
        _MimeTypes.Add("tiff", "image/tiff")
        _MimeTypes.Add("tif", "image/tiff")
        _MimeTypes.Add("svg", "image/svg+xml")
        _MimeTypes.Add("svgz", "image/svg+xml")

        'archives
        _MimeTypes.Add("zip", "application/zip")
        _MimeTypes.Add("rar", "application/x-rar-compressed")
        _MimeTypes.Add("exe", "application/x-msdownload")
        _MimeTypes.Add("msi", "application/x-msdownload")
        _MimeTypes.Add("cab", "application/vnd.ms-cab-compressed")

        ' audio/video
        _MimeTypes.Add("mp3", "audio/mpeg")
        _MimeTypes.Add("qt", "video/quicktime")
        _MimeTypes.Add("mov", "video/quicktime")


        ' adobe
        _MimeTypes.Add("pdf", "application/pdf")
        _MimeTypes.Add("psd", "image/vnd.adobe.photoshop")
        _MimeTypes.Add("ai", "application/postscript")
        _MimeTypes.Add("eps", "application/postscript")
        _MimeTypes.Add("ps", "application/postscript")


        'ms office
        _MimeTypes.Add("doc", "application/msword")
        _MimeTypes.Add("rtf", "application/rtf")
        _MimeTypes.Add("xls", "application/vnd.ms-excel")
        _MimeTypes.Add("ppt", "application/vnd.ms-powerpoint")


        '// open office
        _MimeTypes.Add("odt", "application/vnd.oasis.opendocument.text")
        _MimeTypes.Add("ods", "application/vnd.oasis.opendocument.spreadsheet")
    End Sub

#End Region

#Region "ResponseText"

    Public ReadOnly Property ResponseText As String
        Get
            Dim sr As New StreamReader(_memoryStreamOut)
            sr.BaseStream.Position = 0
            Return sr.ReadToEnd
        End Get
    End Property

#End Region

#Region "Function Write"
    <DispId(1)> _
    Public Function Write(Text As String) As Integer
        _sw.Write(Text)
        _sw.Flush()
        Return Text.Length
    End Function
    <DispId(2)>
    Public Function WriteLine(Text As String) As Integer
        _sw.Write(Text & vbCrLf)
        _sw.Flush()
        Return (Text & vbCrLf).Length
    End Function
    <DispId(62)>
    Public Function WriteStream(SourceStream As System.IO.Stream) As Integer
        _processor.WriteSuccess(_mimeType)
        SourceStream.CopyTo(_memoryStreamOut)
        Return SourceStream.Length
    End Function

    <DispId(63)>
    Public Sub Test()

    End Sub
#End Region

#Region "Redirect"
    <DispId(20)> _
    Public Sub Redirect(RedirectURL As String)
        _IsRedirect = True
        _RedirectURL = RedirectURL
    End Sub
#End Region

#Region "Sub Flush"
    <ComVisible(False)> _
    Public Sub Flush()
        Try
            If _IsRedirect Then
                WriteResponseRedirectHeader()
            Else
                WriteResponseOKHeader()
            End If

            _IsRedirect = False
            _RedirectURL = ""
            _memoryStreamOut.Position = 0
            _memoryStreamOut.CopyTo(_responseStream)
        Catch ex As Exception
        End Try

    End Sub

    Public Sub FlushOptions()
        _responseStream.WriteText("HTTP/1.1 200 OK" & vbCrLf)
        _responseStream.WriteText("Access-Control-Allow-Origin : * " & vbCrLf)
        _responseStream.WriteText("Access-Control-Allow-Methods : POST, GET, OPTIONS" & vbCrLf)
        _responseStream.WriteText("Access-Control-Allow-Headers : origin, x-csrftoken, content-type, accept, accept-encoding, accept-language, host, referer, user-agent, authorization, token" & vbCrLf)
        _responseStream.WriteText("Connection : Close " & vbCrLf & vbCrLf)
    End Sub

#End Region

#Region "Dispose"
    Public Sub Dispose()
        Try
            _MimeTypes.Clear()
            _memoryStreamOut.Close()
            _memoryStreamOut.Dispose()
            _sw.Close()
            _sw.Dispose()
            _responseStream.Close()
            _responseStream.Dispose()
        Catch ex As System.StackOverflowException
        End Try

    End Sub

#End Region

#Region "Sub SendFile"
    <DispId(4)> _
    Public Sub SendFile(FullPath As String, Optional MimeType As String = "")
        ' file is just queed
        _fileToSend = FullPath
        If MimeType = "" Then
            SetMimeType(GetMimeType(FullPath))
        Else
            SetMimeType(MimeType)
        End If
    End Sub

#End Region

#Region "Sub SetMimeType"
    <DispId(5)> _
    Public Sub SetMimeType(MimeType As String)
        _mimeType = MimeType
    End Sub
#End Region

#Region "Sub AddCookie"
    <DispId(6)> _
    Public Sub AddCookie(Name As String, Value As String)
        _cookies.Add(New KeyValuePair(Of String, String)(Name, Value))
    End Sub
#End Region

#Region "Clear"
    <DispId(7)> _
    Public Sub Clear()
        _memoryStreamOut.SetLength(0)
    End Sub
#End Region

#Region "Sub SendPhysicalFile"
    <DispId(16)>
    Public Sub SendPhysicalFile(Filename As String, ResponseStream As GusNet.GusServer.GusOutputStream)
        '_mimeType = GetMimeType(Filename)

        _processor.WriteSuccess(_mimeType)
        Dim Fi As New FileInfo(Filename)
        Dim fileStream As IO.FileStream = Fi.OpenRead()
        fileStream.CopyTo(ResponseStream)
        fileStream.Close()
        fileStream.Dispose()
    End Sub
#End Region

#Region "Privates"

#Region "Sub WriteResponseOKHeader"
    Private Sub WriteResponseOKHeader()
        _responseStream.WriteLine("HTTP/1.1 200")
        _responseStream.WriteLine("Server: Dynateam Web Server")
        _responseStream.WriteLine("Content-Length: " & (_memoryStreamOut.Length).ToString)
        _responseStream.WriteLine("Connection : Close")
        _responseStream.WriteLine("Access-Control-Allow-Origin: * ")
        For Each cookie In _cookies
            _responseStream.WriteLine("Set-Cookie:" & cookie.Key & "=" & cookie.Value)
        Next
        _responseStream.WriteLine("Content-Type: text/html")
        _responseStream.WriteLine("")
    End Sub
#End Region
#Region "Sub WriteResponseRedirectHeader"
    Private Sub WriteResponseRedirectHeader()
        Dim redirectHTML As String = "<html><head></head><body onload=""window.location.href = '" + _RedirectURL + "';""></body><html>"
        _responseStream.WriteLine("HTTP/1.1 200")
        _responseStream.WriteLine("Content-Length: " & redirectHTML.Length.ToString)
        _responseStream.WriteLine("Access-Control-Allow-Origin: * ")
        _responseStream.WriteLine("Content-Type: text/html")
        _responseStream.WriteLine("")
        _responseStream.WriteLine(redirectHTML)
    End Sub

#End Region
#Region "Sub WriteFileNotFoundHeader"
    Public Sub WriteFileNotFoundHeader()
        _responseStream.WriteLine("HTTP/1.1 404 Not Found")
        _responseStream.WriteLine("Content-Length: 0")
        _responseStream.WriteLine("Connection : Close")
        _responseStream.WriteLine("")
    End Sub
#End Region
#Region "Function GetMimeType"
    Public Function GetMimeType(Filename As String) As String

        Dim fi As New FileInfo(Filename)
        Dim Extension As String = fi.Extension
        Extension = Extension.Replace(".", "")

        If _MimeTypes.ContainsKey(Extension) Then
            Return _MimeTypes(Extension)
        Else
            Return "application/x-msdownload"
        End If
    End Function
#End Region


#End Region

End Class



