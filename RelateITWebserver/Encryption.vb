﻿<ComClass(Encryption.ClassId, Encryption.InterfaceId, Encryption.EventsId)> _
Public Class Encryption

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "4455bfd1-21f4-43c5-b112-c096968d86da"
    Public Const InterfaceId As String = "841d673d-b558-4a72-9ca4-195ef195f522"
    Public Const EventsId As String = "dfdc3a8b-0255-4432-abcd-a65d4cb09a82"
#End Region

    ' A creatable COM class must have a Public Sub New() 
    ' with no parameters, otherwise, the class will not be 
    ' registered in the COM registry and cannot be created 
    ' via CreateObject.
    Public Sub New()
        MyBase.New()
    End Sub

    Public Function XOREncrypt(s As String, pw As String)
        Try

        Dim a As Integer = 0
        Dim ResultString As String = ""
        Dim TextLen As Integer = s.Length
        Dim pwLen As Integer = pw.Length
        For i As Integer = 0 To TextLen - 1
            a = Microsoft.VisualBasic.AscW(s(i))
            Dim p As Integer = Microsoft.VisualBasic.AscW(pw(i Mod pwLen))
            a = a Xor (p)
            Dim b As String = a.ToString
            While b.ToString.Length < 3
                b = "0" & b
            End While
            ResultString &= b
        Next
            Return ResultString
        Catch ex As Exception
            Return ""
        End Try

    End Function


    Public Function XORDecrypt(s As String, pw As String) As String

        Try
            Dim resultString As String = ""
            Dim resultHolder As String = ""
            Dim a As Integer = 0
            Dim TextLen As Integer = s.Length
            Dim pwLen As Integer = pw.Length
            Dim i As Integer = 0
            While i < s.Length - 2
                resultHolder = s(i) + s(i + 1) + s(i + 2)
                If s(i) = "0" Then
                    resultHolder = s(i + 1) + s(i + 2)
                End If
                If (s(i) = "0") And (s(i + 1) = "0") Then
                    resultHolder = s(i + 2)
                End If
                a = Integer.Parse(resultHolder)
                Dim p As Integer = Microsoft.VisualBasic.AscW(pw(i / 3 Mod pwLen))
                a = a Xor (p)
                resultString = resultString & Microsoft.VisualBasic.Chr(a)
                i += 3
            End While
            Return resultString
        Catch ex As Exception
            Return ""  ' undgå at den dør hvis keyt er forkert format
        End Try
    End Function


End Class



