﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports HttpMultipartParser
Imports System.Collections.Generic
Imports System.Security
Imports Microsoft.Win32

'Imports RelateIT.StringExtensions

'https://github.com/Vodurden/Http-Multipart-Data-Parser
'https://gusnet.codeplex.com/

<Assembly: AllowPartiallyTrustedCallers>
<ComClass(HTTPServer.ClassId, HTTPServer.InterfaceId, HTTPServer.EventsId)> _
Public Class HTTPServer
    Inherits GusNet.GusServer.GusMainServer
    Public Shared LogMessages As New SynchronizedCollection(Of LogMessage) '¨shared instances of this type is threadsafe
    Private LogThread As New System.Threading.Thread(AddressOf Logger)
    Private AliveThread As New System.Threading.Thread(AddressOf CheckAlive)
#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "fb1c22ea-9a6b-425a-aaf3-de810d87f9bd"
    Public Const InterfaceId As String = "5ba62c0e-eefb-4dff-ab83-a657d5af44a3"
    Public Const EventsId As String = "f6e9a01e-f5a4-45ec-9b47-e70e55c248f2"
#End Region
#Region "Event OnRequest"
    <DispId(10)>
    Public Event OnRequest(Request As HTTPRequest, Response As HTTPResponse, ByRef Success As Boolean)
#End Region
#Region "Event OnFileRequest"
    <DispId(11)>
    Public Event OnFileRequest(Path As String, Filename As String, ByRef Success As Boolean)
#End Region
#Region "Event OnAfterFileSent"
    <DispId(15)>
    Public Event OnAfterFileSent(Path As String, Filename As String, ByRef Success As Boolean)
#End Region
#Region "Event OnServerStartet"
    <DispId(150)>
    Public Event OnServerStartet()
#End Region
#Region "Event OnServerStopped"
    <DispId(151)>
    Public Event OnServerStopped()
#End Region
#Region "Event IsAlive"
    <DispId(152)>
    Public Event OnCheckAlive(ByRef IsAlive As Boolean)
#End Region
#Region "Property RootDirectory"
    <DispId(1)>
    Private Property _RootDirectory As String = ""
    Public Property RootDirectory As String
        Get
            Return _RootDirectory
        End Get
        Set(value As String)
            If value(value.Length - 1) <> "\" Then
                _RootDirectory = value & "\"
            Else
                _RootDirectory = value
            End If
        End Set
    End Property
#End Region
#Region "Property Port"
    <DispId(2)>
    Private Property _Port As Integer = 80
    Public Shadows Property Port As Integer
        Get
            Return MyBase.Port
        End Get
        Set(value As Integer)
            MyBase.Port = value
        End Set
    End Property
#End Region
#Region "Property MaxPostSize"
    <DispId(12)>
    Public Overloads Property MaxPostSize As Integer
        Get
            Return MyBase.MaxPostSize
        End Get
        Set(value As Integer)
            MyBase.MaxPostSize = value
        End Set
    End Property

#End Region
#Region "Property EnableLogging"


    Private _enableLog As Boolean = False
    <DispId(124)>
    Public Property EnableLogging As Boolean
        Get
            Return _enableLog
        End Get
        Set(value As Boolean)
            _enableLog = value
        End Set
    End Property



#End Region
#Region "Property LogFilename"
    Private _LogFilename As String = ""
    <DispId(125)>
    Public Property LogFilename As String
        Get
            Return _LogFilename
        End Get
        Set(value As String)
            _LogFilename = value
        End Set
    End Property
#End Region
#Region "Property EnableSSL"
    Private _EnableSSL As Boolean
    <DispId(135)>
    Public Property EnableSSL As Boolean
        Get
            Return MyBase.IsSsl
        End Get
        Set(value As Boolean)
            MyBase.IsSsl = value
        End Set
    End Property
#End Region
#Region "Property CertificateFilename"
    ' Private _CertificateFilename As String
    <DispId(136)>
    Public Property CertificateFilename As String
        Get
            Return MyBase.CertFile
        End Get
        Set(value As String)
            MyBase.CertFile = value
        End Set
    End Property
#End Region
#Region "Property CertificatePassword"
    <DispId(1186)>
    Public Property CertificatePassword As String
        Get
            Return MyBase.CertPassword
        End Get
        Set(value As String)
            MyBase.CertPassword = value
        End Set
    End Property
#End Region
#Region "Sub New"
    Public Sub New()
        MyBase.New(0, False, "")
        MyBase.MaxPostSize = 10 * 1024 * 1024
    End Sub
#End Region
#Region "Sub StartServer"
    <DispId(3)>
    Public Shadows Sub StartServer()

        '  lav et tjek op noget licence
        '  create custom er registry key
        'CheckRegistrationKey() check noget andet istedet.

        isShutDown = False
        MyBase.Start()
        LogThread.Start()

        ' AliveThread.Start() systes at være en dårlig ide, når man comiler i NAV så¨dør den


        WriteSystemLog("RelateIT Webserver AddIn")
        RaiseEvent OnServerStartet()
    End Sub
#End Region
#Region "Sub StopServer"
    <DispId(4)>
    Public Shadows Sub StopServer()
        Try
            isShutDown = True
            MyBase.Stop()
            LogThread.Abort()
            LogThread.Join()
            AliveThread.Abort()
            AliveThread.Join()


        Catch ex As Exception
        End Try
        RaiseEvent OnServerStopped()
    End Sub
#End Region
#Region "WriteLog"
    <DispId(130)>
    Public Sub WriteLog(Text As String)
        Try
            Dim msg As String = DateTime.Now.ToString() & "     : " & Text & vbCrLf
            LogMessages.Add(New LogMessage With {.Text = msg, .FilePath = LogFilename})
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Write System Log"
    Public Sub WriteSystemLog(Text As String)
        If _enableLog Then
            Try
                Dim msg As String = DateTime.Now.ToString() & "[SYS]: " & Text & vbCrLf
                LogMessages.Add(New LogMessage With {.Text = msg, .FilePath = LogFilename})
            Catch ex As Exception
            End Try
        End If
    End Sub
#End Region


#Region "Alive Thread"
    Private isShutDown As Boolean = False
    Private Sub CheckAlive()
        Dim IsAlive As Boolean = False
        While True And Not isShutDown
            Try
                IsAlive = False
                RaiseEvent OnCheckAlive(IsAlive)
                If IsAlive Then
                    System.Threading.Thread.Sleep(3000)
                Else
                    ShutDownGracefully()
                End If
            Catch ex As Exception
                ShutDownGracefully()
            End Try
        End While
    End Sub
#End Region


#Region ""
    ' Threadsafe File Logger
    Private Sub Logger()
        While True
            Try
                If _enableLog Then
                    For Each Message In LogMessages
                        File.AppendAllText(Message.FilePath, Message.Text)
                    Next
                    LogMessages.Clear()
                End If
                System.Threading.Thread.Sleep(500)
            Catch ex As Exception
            End Try
        End While
    End Sub
#End Region

#Region "Sub HandleRequest"
    <ComVisible(False)>
    Overrides Sub HandleRequest(Processor As GusNet.GusServer.GusHttpProcessor)
        Try
            Dim success As Boolean
            Dim UTF8Encoder As New System.Text.UTF8Encoding
            Dim OrigRequest As String = UTF8Encoder.GetString(Processor.OriginalRequest)
            Dim response As New HTTPResponse(Processor)
            Dim request As New HTTPRequest(Processor)
            'Process GET Request
            'SKAL LAVES OM , så den altid tjekker om en fil findes ..
            ' 1 raise event
            ' hvi filen findes, så senders den,


            If InStr(Processor.SourceUrl, "__KILL__") Then
                WriteSystemLog("Server was killed with '__KILL__' command.")
                StopServer()
            ElseIf InStr(Processor.SourceUrl, "__ISALIVE__") Then
                response.SetMimeType("text/plain")
                response.Write("1")
                response.Flush()
            ElseIf (Processor.Method = "GET") Then
                If IsFileRequest(Processor.SourceUrl) Then



                    ' 1. send physical file
                    Dim FileName As String = Trim(Processor.SourceUrl.Replace("/", "\"))
                    FileName = SelectStr2(1, FileName, "?")       'nts\metro.woff?izvoei // some fonts use e ? to the last

                    If FileName(0) = "\" Then FileName = FileName.Remove(0, 1)

                    FileName = StringFunctions.URLDecode(FileName)


                    Dim PhysicalPath As String = Trim(_RootDirectory) + Trim(FileName)
                    PhysicalPath = PhysicalPath.Replace("\\", "\")

                    Dim PhysicalFile As New FileInfo(PhysicalPath)

                    'Prevent sending files from outside rootfolder
                    If IsSubPathOf(PhysicalFile.FullName, RootDirectory) Then
                        Try
                            success = False
                            RaiseEvent OnFileRequest(_RootDirectory.ToAscii, FileName.ToAscii, success)
                        Catch ex As Exception
                            DumpException(ex)
                        End Try

                        If Not success Then Halt()

                        If IO.File.Exists(PhysicalPath) Then
                            response.SetMimeType(response.GetMimeType(PhysicalPath))
                            response.SendPhysicalFile(PhysicalPath, Processor.OutputStream)
                            Try
                                success = False
                                RaiseEvent OnAfterFileSent(_RootDirectory.ToAscii, FileName.ToAscii, success)
                            Catch ex As Exception
                                DumpException(ex)
                            End Try
                            WriteSystemLog("File served :" + _RootDirectory.ToAscii + FileName.ToAscii)
                            If Not success Then Halt()
                        Else
                            WriteSystemLog("File not found or permission error :" + PhysicalPath)
                            response.WriteFileNotFoundHeader()
                        End If
                    Else
                        response.WriteFileNotFoundHeader()
                    End If


                Else
                        '2. redirect response to NAV

                        Try
                            success = False
                            RaiseEvent OnRequest(request, response, success)
                        Catch ex As Exception
                            DumpException(ex)
                        End Try
                        If Not success Then Halt()

                        If response.IsFileResponse Then
                            response.SendPhysicalFile(response.FileName, Processor.OutputStream)
                            Try
                                success = False
                                RaiseEvent OnAfterFileSent(_RootDirectory.ToAscii, response.FileName.ToAscii, success)
                            Catch ex As Exception
                                DumpException(ex)
                            End Try
                            If Not success Then Halt()
                        Else
                            response.Flush()
                        End If
                    End If
                ElseIf Processor.Method = "OPTIONS" Then 'Process OPTIONS Request
                    WriteSystemLog("OPTIONS REQUEST SERVERD")
                    response.FlushOptions()
                Else
                    'Process POST Request
                    If Not Processor.SourceUrl.Contains("ripple") Then ' ripple hotfix
                    If Processor.RequestHeaders.ContainsKey("Content-Type") Then
                        If Processor.RequestHeaders("Content-Type").ToLower.Contains("multipart") Then
                            Dim parser As MultipartFormDataParser
                            Dim fs As FileStream
                            Try
                                fs = File.OpenRead(Processor.PostDataFile)
                                Dim ec As New System.Text.UTF8Encoding
                                Dim u As String = System.Text.Encoding.UTF8.GetString(Processor.OriginalRequest)
                                parser = New MultipartFormDataParser(fs)
                                For Each item In parser.Parameters
                                    request.Parameters.AddParameter(item.Name, item.Data, False, False, True, False, False)
                                Next
                                If parser.Files.Count > 0 Then
                                    request.Parameters.AddParameter("filecount", parser.Files.Count.ToString, False, False, True, False, False)
                                End If
                                Dim i As Integer = 1
                                For Each file In parser.Files
                                    Dim tmpfilename As String = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString()
                                    Dim fi As New FileInfo(tmpfilename)
                                    Dim filestream As IO.FileStream = fi.OpenWrite()
                                    ' hvis filename = "blob" så skal vi selv sikre at efternavn er ."jpg", hmmm det gør vi i  navn
                                    file.Data.CopyTo(filestream)
                                    filestream.Close()
                                    request.Parameters.AddParameter("filename_" + i.ToString(), file.FileName.Replace("\", "\\"), False, False, True, False, True)
                                    request.Parameters.AddParameter("tempfilename_" + i.ToString(), tmpfilename.Replace("\", "\\"), False, False, True, False, True)
                                    i += 1
                                Next
                                fs.Close()
                            Catch ex As Exception
                                DumpException(ex)
                            End Try
                        End If
                    End If


                    '2. redirect response to NAV
                    Try
                        success = False
                        RaiseEvent OnRequest(request, response, success)
                    Catch ex As Exception
                        WriteSystemLog("Exception")
                        WriteSystemLog(ex.Message)
                    End Try

                    If Not success Then Halt()

                    If response.IsFileResponse Then
                        response.SendPhysicalFile(response.FileName, Processor.OutputStream)
                        Try
                            success = False
                            RaiseEvent OnAfterFileSent(_RootDirectory.ToAscii, response.FileName.ToAscii, success)
                        Catch ex As Exception
                            DumpException(ex)
                        End Try
                        If Not success Then Halt()
                    Else
                        response.Flush()
                    End If

                End If
            End If


            UTF8Encoder = Nothing
            'clean up
            response.Dispose()
            request.Dispose()
            Processor = Nothing
            response = Nothing
            request = Nothing

        Catch ex As Exception
            DumpException(ex)
        End Try

    End Sub
#End Region
#Region "Function IsFileRequest"
    Private Function IsFileRequest(path As String) As Boolean

        ' . kan være en del af en postparameter...
        ' hmmm. username en del af requestebdd h,m,mm poist
        'parmIndex = path.IndexOf("?")
        Return path.IndexOf(".") > 0

    End Function
#End Region

#Region "IsSubPathOf"
    Private Function IsSubPathOf(FilePath As String, RootDir As String) As Boolean
        If (FilePath.ToLower.Substring(0, RootDir.ToLower.Length) = RootDir.ToLower) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

    ' depreached
#Region "Halt"


    Private Sub ShutDownGracefully()
        Try
            WriteLog("Client dead!, Gracefylly shutting down server")
            System.Threading.Thread.Sleep(1000)

            ' Stop Logger Thread
            LogThread.Abort()
            LogThread.Join()

            MyBase.Stop()

            isShutDown = True

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub Halt()

        ' This function is depreached

        ' Navision Crasher




        '//13-01-2017 kode udkoemmenteret, da der ingen grund er til at server stopper fordi applikatino laver fejl.
        ' skal det evt være en parametersttyre option... ?? ShotdownOnError ... da de gamle NAS er jo fejler.. d
        '//26-02-2017 .. når vi halter er der opstået ene fejl i NAV, 

        ' Stop server, and threads..
        Try
            ' LogThread.Abort()
            ' LogThread.Join()
            ' MyBase.Stop()
        Catch ex As Exception
        End Try

        '//Raise Navision Event to Notify it before we crash it
        Try
            ' RaiseEvent OnServerStopped()
        Catch ex As Exception

        End Try

        'Generate new exception to crash application. Service Tier should restart server.
        ' hmm but exceptions bliver jo håndtere i NAV nu...

        'Throw New Exception("Application Halted")

    End Sub


#End Region



    Public Sub DumpException(ex As Exception)
        WriteSystemLog("Exception" & vbCrLf)
        WriteSystemLog(ex.Message & vbCrLf)
        If Not ex.InnerException Is Nothing Then
            WriteSystemLog(ex.InnerException.Message() & vbCrLf)
        End If
    End Sub
End Class
