﻿Imports System.Runtime.InteropServices
Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.IO

<ComVisible(False)> _
Public Module StringFunctions

#Region "Function SelectStr2"
    Public Function SelectStr2(Index As Integer, Value As String, Separator As String) As String
        Dim result As String = ""
        Dim Counter As Integer = 0
        Value &= Separator
        For i As Integer = 0 To Value.Length - 1
            If Value(i) = Separator Then
                Counter += 1
                If Counter = Index Then
                    Return result
                Else
                    result = ""
                End If
            Else
                result &= Value(i)
            End If
        Next
        Return result
    End Function

#End Region

#Region "Function SelectCount"
    Public Function SelectCount(Value As String, Separator As String) As String
        Dim Counter As Integer = 0
        Value &= Separator
        For i As Integer = 0 To Value.Length - 1
            If Value(i) = Separator Then
                Counter += 1
            End If
        Next
        Return Counter
    End Function
#End Region

#Region "Function  ToAscii"
    <Extension()> _
    Public Function ToAscii(unicodeString As String) As String
        '// Create two different encodings.
        Dim ascii As Encoding = Encoding.ASCII
        Dim unicode As Encoding = Encoding.Unicode

        '// Convert the string into a byte array.
        Dim unicodeBytes As Byte() = unicode.GetBytes(unicodeString)

        '// Perform the conversion from one encoding to the other.
        Dim asciiBytes As Byte() = Encoding.Convert(unicode, ascii, unicodeBytes)

        '// Convert the new byte[] into a char[] and then into a string. 
        Dim asciiChars(ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)) As Char
        ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0)
        Return New String(asciiChars)
    End Function
#End Region

#Region "Function  ToAscii"
    <Extension()> _
    Public Sub CopyTo(Input As Stream, Destination As Stream)
        Dim Buffer(Input.Length) As Byte
        Dim BytesRead As Integer
        BytesRead = Input.Read(Buffer, 0, Input.Length)
        While (BytesRead > 0)
            Destination.Write(Buffer, 0, BytesRead)
            BytesRead = Input.Read(Buffer, 0, Input.Length)
        End While
    End Sub
#End Region

    'reference: https://weblog.west-wind.com/posts/2009/Feb/05/Html-and-Uri-String-Encoding-without-SystemWeb
    Public Function URLDecode(text As String) As String
        Return System.Uri.UnescapeDataString(text)
    End Function


End Module

