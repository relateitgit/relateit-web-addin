﻿Imports System.Runtime.InteropServices
'Imports DynateamWebserver.StringExtensions
<ComClass(HTTPParameters.ClassId, HTTPParameters.InterfaceId, HTTPParameters.EventsId)> _
Public Class HTTPParameters

#Region "COM GUIDs"
    ' These  GUIDs provide the COM identity for this class 
    ' and its COM interfaces. If you change them, existing 
    ' clients will no longer be able to access the class.
    Public Const ClassId As String = "17b688bc-b47b-45a9-aa3d-4823da4c23bf"
    Public Const InterfaceId As String = "70d8c13c-8b3d-4d60-a585-45a63a4bec88"
    Public Const EventsId As String = "5dfb279e-dd00-40e5-965d-431ca6da135c"
#End Region

    Private _Parameters As New List(Of KeyValuePair(Of String, String))
    Private _COOKIEMarkers As New List(Of KeyValuePair(Of String, Boolean))
    Private _GETMarkers As New List(Of KeyValuePair(Of String, Boolean))
    Private _POSTMarkers As New List(Of KeyValuePair(Of String, Boolean))
    Private _HEADERMarkers As New List(Of KeyValuePair(Of String, Boolean))
    Private _FILEMarkers As New List(Of KeyValuePair(Of String, Boolean))
    Private _Cursor As Integer = 0

#Region "Property Count"
    <DispId(1)> _
    Public ReadOnly Property Count As Integer
        Get
            Return _Parameters.Count
        End Get
    End Property
#End Region

#Region "Property Key"
    <DispId(2)> _
    Public ReadOnly Property Key As String
        Get
            Return _Parameters(_Cursor).Key
        End Get
    End Property
#End Region

#Region "Property Value"
    <DispId(3)> _
    Public ReadOnly Property Value As String
        Get
            Return _Parameters(_Cursor).Value
        End Get
    End Property
#End Region

#Region "Is Cookie"
    <DispId(13)> _
    Public ReadOnly Property IsCookie As String
        Get
            Return _COOKIEMarkers(_Cursor).Value
        End Get
    End Property
#End Region

#Region "Sub New"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region

#Region "Sub AddParameter"
    <DispId(4)> _
    Public Sub AddParameter(Key As String, Value As String, IsCookie As Boolean, IsGET As Boolean, isPOST As Boolean, isHeader As Boolean, isFile As Boolean)
        If Key = "" Then Exit Sub
        _Parameters.Add(New KeyValuePair(Of String, String)(Key, Value))

        _COOKIEMarkers.Add(New KeyValuePair(Of String, Boolean)(Key, IsCookie))
        _GETMarkers.Add(New KeyValuePair(Of String, Boolean)(Key, IsGET))
        _POSTMarkers.Add(New KeyValuePair(Of String, Boolean)(Key, isPOST))
        _HEADERMarkers.Add(New KeyValuePair(Of String, Boolean)(Key, isHeader))
        _FILEMarkers.Add(New KeyValuePair(Of String, Boolean)(Key, isFile))
    End Sub
#End Region

#Region "Sub GetParameter"
    <DispId(7)> _
    Public Function GetParameter(key As String) As String
        For Each item In _Parameters
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return ""
    End Function
#End Region

#Region "Get Markers"
    <DispId(17)> _
    Public Function GetCookieMarker(key As String) As Boolean
        For Each item In _COOKIEMarkers
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return False
    End Function


    <DispId(27)> _
    Public Function GetGETMarker(key As String) As Boolean
        For Each item In _GETMarkers
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return False
    End Function

    <DispId(37)> _
    Public Function GetPOSTMarker(key As String) As Boolean
        For Each item In _POSTMarkers
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return False
    End Function

    <DispId(47)> _
    Public Function GetHEADERMarker(key As String) As Boolean
        For Each item In _HEADERMarkers
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return False
    End Function

    <DispId(57)> _
    Public Function GetFILEMarker(key As String) As Boolean
        For Each item In _FILEMarkers
            If item.Key = key Then
                Return item.Value
            End If
        Next
        Return False
    End Function
#End Region

#Region "Sub MoveFirst"
    <DispId(5)> _
    Public Sub MoveFirst()
        _Cursor = 0
    End Sub
#End Region

#Region "Sub MoveLast"
    <DispId(6)> _
    Public Sub MoveNext()
        _Cursor += 1
    End Sub
#End Region

#Region "Dispose"

    Public Sub Dispose()
        _Parameters.Clear()
        _COOKIEMarkers.Clear()
        _Parameters = Nothing
        _COOKIEMarkers = Nothing
    End Sub
#End Region

End Class


