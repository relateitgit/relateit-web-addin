﻿Imports Microsoft.Win32
Imports RelateITWebserver

Module Module1
    Private WithEvents RelateITWebserver As RelateITWebserver.HTTPServer

    Sub Main()

        Dim root = IO.Path.GetDirectoryName(Diagnostics.Process.GetCurrentProcess().MainModule.FileName)
        Dim port As Integer = 8046

        If My.Application.CommandLineArgs.Count > 0 Then
            Try
                port = CInt(My.Application.CommandLineArgs(0))
            Catch ex As Exception
            End Try
        End If

        RelateITWebserver = New RelateITWebserver.HTTPServer
        With RelateITWebserver
            .RootDirectory = root
            .EnableLogging = True
            .LogFilename = .RootDirectory + "log.txt"
            .Port = port
            .StartServer()
        End With

        Console.ReadKey()
        RelateITWebserver.StopServer()


    End Sub

    Private Sub RelateITWebserver_OnAfterFileSent(Path As String, Filename As String, ByRef Success As Boolean) Handles RelateITWebserver.OnAfterFileSent
        Success = True
    End Sub

    Private Sub RelateITWebserver_OnCheckAlive(ByRef IsAlive As Boolean) Handles RelateITWebserver.OnCheckAlive
        IsAlive = True
    End Sub

    Private Sub RelateITWebserver_OnFileRequest(Path As String, Filename As String, ByRef Success As Boolean) Handles RelateITWebserver.OnFileRequest
        Success = True
    End Sub

    Private Sub RelateITWebserver_OnRequest(Request As HTTPRequest, Response As HTTPResponse, ByRef Success As Boolean) Handles RelateITWebserver.OnRequest
        Success = True
    End Sub

    Private Sub RelateITWebserver_OnServerStartet() Handles RelateITWebserver.OnServerStartet
        Console.WriteLine("RelateIT Webserver startet on localhost: " + RelateITWebserver.Port.ToString)
        Console.WriteLine("Press any key to quit")
    End Sub

    Private Sub RelateITWebserver_OnServerStopped() Handles RelateITWebserver.OnServerStopped
        Console.WriteLine("Server stopped")
    End Sub
End Module

